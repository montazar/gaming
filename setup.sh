#!/usr/bin/env bash

this="$(echo "$0" | cut -d/ -f$(($(echo "$0" | grep -o '/' | wc -l) + 1)))"

function __message() {
    local color="$1"
    local message="$2"
    local output_file="${this}.progress"

    case $color in
    "green" | "GREEN")
        COLOR="32m"
        ;;
    "red" | "RED")
        COLOR="31m"
        ;;
    "yellow" | "YELLOW")
        COLOR="33m"
        ;;
    esac

    echo -e "\e[1;${COLOR}SCRIPT: \e[0m${message}" | tee -a "$output_file"
}

__install() {
    local pkgs="$1"
    local len
    len="$(echo "$pkgs" | wc -w)"

    local output_file="${this}.progress"

    while [ "$len" -gt 0 ]; do
        current=$(echo "$pkgs" | cut -d ' ' -f "$len" | cut -f 1)
        len=$((len - 1))

        if ! dpkg -l | grep -qE "ii \s+$pkgs\s+"; then
            echo -e "\e[1;33mInstalling $current\e[0m"

            if sudo DEBIAN_FRONTEND=noninteractive apt install -y "$current"; then
                echo -e "\e[1;32mInstalled $current\e[0m" | tee -a "$output_file"
            else
                echo -e "\e[1;31mFailed to install $current\e[0m" | tee -a "$output_file"
            fi
        fi

    done
}

if [ "$USER" = "root" ]; then
    echo 'You should not run this script as root user.'; exit 1
fi

__message "NOTICE" "Adding preferences for gaming..."

if __installed apt; then
    sudo dpkg --add-architecture i386
    sudo apt update
elif __installed pacman; then
    sudo sed -i '/#[multilib]/s/^#//' /etc/pacman.conf
    sudo pacman -Syu
fi

if lspci -k | grep -EA3 'VGA|3D|Display' | grep -iq nvidia; then
    __message "NOTICE" "Proceeding with Nvidia."
    if __installed apt; then 
	sudo add-apt-repository -y ppa:graphics-drivers/ppa
	sudo apt update
	__install "nvidia-driver-450 libnvidia-gl-450 libnvidia-gl-450:i386 libvulkan1 libvulkan1:i386"

    elif __installed pacman; then
	__install "nvidia-dkms nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader"
    fi
    __message "OK" "Drivers installed."
else

    __message "NOTICE" "Assuming AMD/Intel."
    if __installed apt; then 
	sudo add-apt-repository -y ppa:kisak/kisak-mesa
	sudo apt update
	__install "libgl1-mesa-dri:i386 mesa-vulkan-drivers mesa-vulkan-drivers:i386"

    elif __installed pacman; then
	__install "lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader"
	__install "vulkan-intel lib32-vulkan-intel"
    fi
    __message "OK" "Drivers installed."
    
    __message "NOTICE" "Installing CoreCtrl..."
    if __installed apt; then
        sudo add-apt-repository ppa:ernstp/mesarc
        sudo apt-get update
    fi
    if __install "corectrl"; then
        __message "OK" "CoreCtrl installed."
    fi
fi

__message "NOTICE" "Remember to reboot to finish driver installation."


# ACO is a compiler that has proven to increase FPS in games.
__message "NOTICE" "Checking if ACO is already installed..."
if ! grep -qi "RADV_PERFTEST=aco" /etc/environment; then
    echo "RADV_PERFTEST=aco" | sudo tee -a /etc/environment
    __message "NOTICE" "To disable, remove the line from /etc/environment"
    __message "NOTICE" "For single games on Steam, set the launch option: RADV_PERFTEST=aco %command%\nLutris: Enable in settings if you don't use system-wide ACO."

    __message "OK" "ACO enabled."
    fi

    __message "NOTICE" "Installing Wine and its dependencies..."
    if __installed apt; then
	wget -O - https://dl.winehq.org/wine-builds/winehq.key | sudo apt-key add -
	sudo add-apt-repository -y 'deb https://dl.winehq.org/wine-builds/ubuntu/ focal main'
	sudo apt update
	sudo apt-get install -y --install-recommends winehq-staging
	__install "libgnutls30:i386 libldap-2.4-2:i386 libgpg-error0:i386 libxml2:i386 libasound2-plugins:i386 libsdl2-2.0-0:i386 libfreetype6:i386 libdbus-1-3:i386 libsqlite3-0:i386"
    elif __installed pacman; then
	__install "wine-staging giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse libgpg-error lib32-libgpg-error alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo sqlite lib32-sqlite libxcomposite lib32-libxcomposite libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader lutris"
    fi
    __message "OK" "Wine installed."


    __message "NOTICE" "Installing Lutris..."
    sudo add-apt-repository ppa:lutris-team/lutris -y
    __install "lutris"
    __message "OK" "Lutris installed."

    __message "NOTICE" "Enabling Esync..."
    if [ "$(ulimit -Hn)" -lt "1000000" ]; then
	append="DefaultLimitNOFILE=1048576"
	if ! grep -qi "$append" /etc/systemd/system.conf; then
	    echo "$append" | sudo tee -a /etc/systemd/system.conf
	fi

	if ! grep -qi "$append" /etc/systemd/user.conf; then
	    echo "$append" | sudo tee -a /etc/systemd/user.conf
	fi

	append="$(whoami) hard nofile 1048576"
	if ! grep -qi "$append" /etc/security/limits.conf; then
	    echo "$append" | sudo tee -a /etc/security/limits.conf
	fi
    fi
    __message "OK" "Esync enabled."

    __message "NOTICE" "Installing DXVK..."
    if __installed apt; then
	__install "dxvk dxvk-wine32-development dxvk-wine64-development"
    elif __installed pacman; then
	__install "dxvk-bin"
    fi

# To remove GameMode, run the following two commands:
# systemctl --user stop gamemoded.service
# ninja uninstall -C builddir
#
# GameMode attempts to reduce/remove CPU throttling
__message "NOTICE" "Installing GameMode..."
if __installed apt; then
    __install "meson libsystemd-dev pkg-config ninja-build git libdbus-1-dev libinih-dev dbus-user-session"
pushd /tmp
git clone https://github.com/FeralInteractive/gamemode.git
cd gamemode
./bootstrap.sh
elif __installed pacman; then
    __install "gamemode"
fi
__message "NOTICE" "Lutris: Enable in settings\nSteam: Add launch option: gamemoderun %command%\nTerminal: gamemoderun ./game"
__message "NOTICE" "There is an optional GameMode GNOME shell extension."
__message "OK" "GameMode installed."
popd

__install "steam"

# The stock steam proton is rather old and behind the Wine team. Which means there are a lot of performance tweaks and improvements that you aren’t getting yet.
__message "NOTICE" "Installing custom Proton..."
pushd /tmp
wget https://raw.githubusercontent.com/Termuellinator/ProtonUpdater/master/cproton.sh
sudo chmod +x cproton.sh
./cproton.sh
__message "NOTICE" "Steam Play is required to enforce use of this proton version. See Steam settings."
__message "OK" "Proton installed."

__message "NOTICE" "Installing games..."
if [ -f deb.sh ]; then
    ./deb.sh "gaming"
fi
__install "python3-pip"
__message "NOTICE" "Installing truckersmp-cli..."
pip3 install --user truckersmp-cli || pip install --user truckersmp-cli
pip3 install truckersmp-cli --upgrade || pip install truckersmp-cli --upgrade
__message "OK" "Games installed."

__message "NOTICE" "Tuning PulseAudio"
uncomment 'high-priority = yes' '/etc/pulse/daemon.conf' '; '
uncomment 'nice-level = -11' '/etc/pulse/daemon.conf' '; '
uncomment 'realtime-scheduling = yes' '/etc/pulse/daemon.conf' '; '
uncomment 'realtime-priority = yes' '/etc/pulse/daemon.conf' '; '
sudo sed -ri '/^(;\s*)?\s*resample-method\s*=.*/s/.*/resample-method = speex-float-10/' /etc/pulse/daemon.conf

__install "winetricks"
rm -rf ~/.wine
WINEARCH=win32 wine wineboot
winetricks csmt=on grabfullscreen=y isolate_home
winetricks -q corefonts
winetricks -q vcrun2005
winetricks -q vc2005expresssp1
winetricks -q wininet winhttp
wineboot -u

__install 'dxvk-bin'
setup_dxvk install

rm -f ~/.local/share/applications/wine-extension*.desktop
rm -f ~/.local/share/icons/hicolor/*/*/application-x-wine-extension*
rm -f ~/.local/share/applications/mimeinfo.cache
rm -f ~/.local/share/mime/packages/x-wine*
rm -f ~/.local/share/mime/application/x-wine-extension*
update-desktop-database ~/.local/share/applications
update-mime-database ~/.local/share/mime/

# Thanks to Chris Titus Tech (https://christitus.com/ultimate-linux-gaming-guide/)
__message "OK" "Done."
